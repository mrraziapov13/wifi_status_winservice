﻿using System.ServiceProcess;
using System.Threading;
using System.Net.NetworkInformation;
using System.IO;
using System.Threading.Tasks;
using System;

namespace WindowsService1
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Task.Run(() => Processing());
        }

        private void Processing()
        {
            try
            {
                bool pingable = false;
                Ping pinger = new Ping();
                var location = System.Reflection.Assembly.GetEntryAssembly().Location;
                var path = Path.GetDirectoryName(location);
                string file_name = "\\status.txt";
                string full_path = path + file_name;
                while (true)
                {
                    Thread.Sleep(1000);
                    try
                    {
                        PingReply reply = pinger.Send("8.8.8.8");
                        pingable = reply.Status == IPStatus.Success;
                        if (pingable)
                        {
                            write_to_file(1);
                        }
                        else
                        {
                            write_to_file(-1);
                        }
                    }
                    catch (PingException)
                    {
                        write_to_file(-1);
                    }
                }
                void write_to_file(int status)
                {
                    using (StreamWriter sw = new StreamWriter(full_path, false))
                    {
                        sw.WriteLine(status.ToString());
                        sw.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }
            throw new NotImplementedException();
        }

        protected override void OnStop()
        {
        }
    }
}
